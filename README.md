<p style="align: center">
    <h1 align="center">Hi <img src="https://raw.githubusercontent.com/ABSphreak/ABSphreak/master/gifs/Hi.gif" width="30px">, I'm Konstantinos</h1>
</p>

---

### What do I do ?

I'm a Greek guy living in France, working as a software development engineer, and coding in a number of languages but mainly Python and C/C++ (not according to my stats). Don't forget to visit my [GitHub](https://github.com/konstantinosmelis) page where you can find my peronal work.

Also I'm a sailor and a scuba diver.

---

### Links

- My Online website : [konstantinos.melissaratos.com](https://konstantinos.melissaratos.com/)
